<?php
namespace Ekvi\Adminjoins\Block\Adminhtml\Employee\Edit;
use Ekvi\Adminjoins\Model\EmployeeFactory;
use Ekvi\Adminjoins\Model\SalaryFactory;
use Magento\Cms\Model\Wysiwyg\Config;
/**
 * Adminhtml employee edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    protected $_modelSection;
    protected $_wysiwygConfig;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    protected $_countryFactory;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        SalaryFactory $modelSalary,
        Config $wysiwygConfig,
        EmployeeFactory $modelEmployee,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_modelSalary = $modelSalary;
        $this->_modelEmployee = $modelEmployee;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('employee_form');
        $this->setTitle(__('Employee Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Ekvi\Adminjoins\Model\Employee $model */
        $model = $this->_coreRegistry->registry('adminjoins_employee');
        
        /*Salary data*/
        $salaryData=$this->_modelSalary->create()->getCollection()->getData();
        $options=array();
        foreach($salaryData as $val){
            $options [$val['salary_id']]= $val['salary'];
        }
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('post_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'name',
            'text',
            ['name' => 'name', 'label' => __('Employee Name'), 'title' => __('Employee Name'), 'required' => true]
        );
        $fieldset->addField(
            'emailid',
            'text',
            ['name' => 'emailid', 'label' => __('Employee Email'), 'title' => __('Employee Email'), 'required' => true,'class' =>'validate-email',
                'note' =>  'Please enter a valid email address. For example johndoe@domain.com.']
        );
        $fieldset->addField(
            'salary_id',
            'select',
            [
                'name' => 'salary_id',
                'label' => __('Salary'),
                'title' => __('Salary'),
                'required' => true,
                'options'   => $options
            ]
        );
        $data=$model->getData();
        $form->setValues($data);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}