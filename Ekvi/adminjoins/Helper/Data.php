<?php
namespace Ekvi\Adminjoins\Helper;
 
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
 
class Data extends AbstractHelper
{
    const XML_ADMINJOINS_PAGE_TITLE           = 'adminjoins/general/page_title';
    const XML_ADMINJOINS_PAGE_METADESCRIPTION = 'adminjoins/general/meta_description';
    const XML_ADMINJOINS_PAGE_METAKEYWORD     = 'adminjoins/general/meta_keywords';
   
    protected $_scopeConfig;
 
   
    public function __construct(
       Context $context,
       \Magento\Backend\Model\UrlInterface $backendUrl,
       ScopeConfigInterface $scopeConfig
    ) {
       parent::__construct($context);
       $this->_backendUrl = $backendUrl;
       $this->_scopeConfig = $scopeConfig;
    }
    
    public function getAdminjoinsTitle()
    {
        return $this->scopeConfig->getValue(
            self::XML_ADMINJOINS_PAGE_TITLE, ScopeInterface::SCOPE_STORE
        );
    }

    public function getDescription()
    {
        return $this->scopeConfig->getValue(
            self::XML_ADMINJOINS_PAGE_METADESCRIPTION, ScopeInterface::SCOPE_STORE
        );
    }

    public function getMetaKeywords()
    {
        return $this->scopeConfig->getValue(
            self::XML_ADMINJOINS_PAGE_METAKEYWORD, ScopeInterface::SCOPE_STORE
        );
    }
   
 
  
}