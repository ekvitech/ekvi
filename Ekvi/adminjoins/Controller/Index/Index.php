<?php

namespace Ekvi\Adminjoins\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context as Context;
class Index extends \Magento\Framework\App\Action\Action
{
	
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }
	public function execute()
	{
	   	$this->_view->loadLayout();
	    $this->_view->getLayout()->initMessages();
	   	$this->_view->renderLayout();
	}
}