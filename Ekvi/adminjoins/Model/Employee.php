<?php

namespace Ekvi\Adminjoins\Model;
 
use Magento\Framework\Model\AbstractModel;
 
class Employee extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Ekvi\Adminjoins\Model\ResourceModel\Employee');
    }

}