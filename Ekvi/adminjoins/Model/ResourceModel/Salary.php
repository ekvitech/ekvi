<?php
namespace Ekvi\Adminjoins\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;
 
class Salary extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('ekvi_employee_salary', 'salary_id');
    }
}