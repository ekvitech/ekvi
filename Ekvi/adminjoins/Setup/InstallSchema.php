<?php

namespace Ekvi\Adminjoins\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        SchemaSetupInterface $setup, ModuleContextInterface $context
    ) {
        /**
         * Prepare database before module installation
         */
        $setup->startSetup();
         
        /**
         ****** Create table 'ekvi_employee' ****** 
         */
        $table = $setup->getConnection()->newTable(
            $setup->getTable('ekvi_employee')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false,'primary'  => true],
            'Employee Id'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '200',
            ['nullable' => false],
            'Employee Name'
        )->addColumn(
            'emailid',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '200',
            ['nullable' => false],
            'Employee Emailid'
        )->addColumn(
            'salary_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            '2',
            ['nullable' => false],
            'Current Salary'
        );
        $setup->getConnection()->createTable($table);

        /**
        ****** Create table 'ekvi_employee_salary' ****** 
         */
        $table = $setup->getConnection()->newTable(
            $setup->getTable('ekvi_employee_salary')
        )->addColumn(
            'salary_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Salary ID'
        )->addColumn(
            'salary',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '200',
            ['nullable' => false],
            'Salary Group'
        );
         $setup->getConnection()->createTable($table);
       
        

        $setup->endSetup();
    }
}
